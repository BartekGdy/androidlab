package wsb.android.opionionapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpionionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpionionApiApplication.class, args);
	}
}
