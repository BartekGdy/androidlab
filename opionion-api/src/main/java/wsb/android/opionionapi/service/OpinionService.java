package wsb.android.opionionapi.service;

import org.springframework.stereotype.Component;
import wsb.android.opionionapi.mapper.OpinionMapper;
import wsb.android.opionionapi.model.Author;
import wsb.android.opionionapi.model.OpinionDTO;
import wsb.android.opionionapi.model.Rate;
import wsb.android.opionionapi.repository.AuthorRepository;
import wsb.android.opionionapi.repository.RateRepository;
import java.util.Optional;

@Component
public class OpinionService {

    private final AuthorRepository authorRepository;
    private final RateRepository rateRepository;
    private final OpinionMapper mapper;

    public OpinionService(AuthorRepository authorRepository, RateRepository rateRepository, OpinionMapper mapper) {
        this.authorRepository = authorRepository;
        this.rateRepository = rateRepository;
        this.mapper = mapper;
    }

    public OpinionDTO addOpinion(OpinionDTO opinion){
        Author author = getAuthor(opinion);
        Rate rate = getRate(opinion);
        author.addRate(rate);
        rate.addAuthor(author);
        authorRepository.save(author);
        rateRepository.save(rate);
        return mapper.map(author, rate);
    }

    private Author getAuthor(OpinionDTO opinion){
        String firstName = opinion.getAuthor().getFirstName();
        String lastName = opinion.getAuthor().getLastName();
        Optional<Author> author = authorRepository.findByFirstNameAndAndLastName(firstName, lastName);
        return author.orElseGet(opinion::getAuthor);
    }
    private Rate getRate(OpinionDTO opinion){
        float ratePoints = opinion.getRate().getRatePoints();
        String note = opinion.getRate().getNote();
        Optional<Rate> rate = rateRepository.findByRatePointsAndNote(ratePoints,note);
        return rate.orElseGet(opinion::getRate);
    }
}
