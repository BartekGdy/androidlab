package wsb.android.opionionapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wsb.android.opionionapi.model.Rate;

import java.util.Optional;

@Repository
public interface RateRepository extends CrudRepository<Rate,Long>{

    Optional<Rate> findByRatePointsAndNote(float ratePoints, String note);
}
