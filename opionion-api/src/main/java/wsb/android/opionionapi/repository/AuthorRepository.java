package wsb.android.opionionapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wsb.android.opionionapi.model.Author;

import java.util.Optional;

@Repository
public interface AuthorRepository extends CrudRepository<Author,Long>{

    Optional<Author> findByFirstNameAndAndLastName(String firstName, String lastName);
}
