package wsb.android.opionionapi.model;

import lombok.Data;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data

public class OpinionDTO implements Serializable{

    @NotNull
    private Rate rate;
    @NotNull
    private Author author;
}
