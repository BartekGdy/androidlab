package wsb.android.opionionapi.mapper;

import org.springframework.stereotype.Component;
import wsb.android.opionionapi.model.Author;
import wsb.android.opionionapi.model.OpinionDTO;
import wsb.android.opionionapi.model.Rate;

@Component
public class OpinionMapper {

    public OpinionDTO map(Author author, Rate rate){
        OpinionDTO dto = new OpinionDTO();
        dto.setAuthor(author);
        dto.setRate(rate);
        return dto;
    }
}
