package wsb.android.opionionapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wsb.android.opionionapi.model.OpinionDTO;
import wsb.android.opionionapi.service.OpinionService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/opinion")
public class OpinionResource {

    private final OpinionService opinionService;

    public OpinionResource(OpinionService opinionService) {
        this.opinionService = opinionService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OpinionDTO> postOpinion(@RequestBody @Valid OpinionDTO opinion) {
        OpinionDTO opinionDTO = opinionService.addOpinion(opinion);
        return new ResponseEntity<>(opinionDTO, HttpStatus.OK);
    }

}
