package zpum.lab2.labolatorium2;

import java.util.HashMap;
import java.util.Map;

public final class UsersMap {

    private static final UsersMap INSTANCE = new UsersMap();

    private final Map<String,String> usersMap = new HashMap<>();

    public static UsersMap getInstance(){
        return INSTANCE;
    }

    private UsersMap() {
        usersMap.put("Bartek","qwerty");
        usersMap.put("Tomek","qwerty");
    }

    public Map<String, String> getUsersMap() {
        return new HashMap<>(usersMap);
    }

}
