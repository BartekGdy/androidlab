package zpum.lab2.labolatorium2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.editTextLogin);
        password = findViewById(R.id.editTextPassword);
    }

    public void userLogIn(View view) {
        if(isLoginValid()){
            showCalendarPicker();
        }else {
            showWrongCredentialsDialog();
        }
    }

    private boolean isLoginValid() {
        Map<String, String> map =  UsersMap.getInstance().getUsersMap();
        return password.getText().toString().equals(map.get(login.getText().toString()));
    }

    public void showWrongCredentialsDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Błąd logowania");
        alertDialog.setMessage("Wprowadzono błędny login lub hasło");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
    
    public void showCalendarPicker(){
        Intent i = new Intent(getBaseContext(),SecondActivity.class);
        startActivity(i);
    }
}