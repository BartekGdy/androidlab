package lab4.android.wsb.laboratorium4.model;

/**
 * Created by Bartek on 20.06.2018.
 */

public class Rate {

    private float ratePoints;
    private String note;

    public float getRatePoints() {
        return ratePoints;
    }

    public void setRatePoints(float ratePoints) {
        this.ratePoints = ratePoints;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "rate:{" +
                "ratePoints:"+ratePoints +"," +
                "note:"+note +
                "}";
    }
}
