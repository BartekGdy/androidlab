package lab4.android.wsb.laboratorium4;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;

/**
 * Created by Bartek on 20.06.2018.
 */

public class Service {
    private final Context context;
    private final String url = "http://10.0.2.2:8081/opinion";

    public Service(Context context) {
        this.context = context;
    }

    public void postOpinion(final String opinion){
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest postRequest = new StringRequest(
                Request.Method.POST,
                url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(context, "Wysłano opinie", Toast.LENGTH_LONG).show();
                }
                },
                new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return opinion == null ? null : opinion.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };
        queue.add(postRequest);
        queue.start();
    }
}
