package lab4.android.wsb.laboratorium4.model;

/**
 * Created by Bartek on 20.06.2018.
 */

public class Opinion {

    private Author author;
    private Rate rate;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }
}
