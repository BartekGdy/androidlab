package lab4.android.wsb.laboratorium4;

import com.google.gson.Gson;

import lab4.android.wsb.laboratorium4.model.Opinion;

/**
 * Created by Bartek on 20.06.2018.
 */

public class Utils {

    public static String parseToJson(Opinion opinion){
        return new Gson().toJson(opinion);
    }
}
