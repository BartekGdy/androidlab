package lab4.android.wsb.laboratorium4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import lab4.android.wsb.laboratorium4.model.Author;
import lab4.android.wsb.laboratorium4.model.Opinion;
import lab4.android.wsb.laboratorium4.model.Rate;

public class MainActivity extends AppCompatActivity {
    EditText firstName;
    EditText lastName;
    EditText note;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstName = findViewById(R.id.txtFirstName);
        lastName = findViewById(R.id.txtLastName);
        note = findViewById(R.id.txtNote);
        ratingBar = findViewById(R.id.rateId);
    }

    public Opinion getData(){
        Author author = new Author();
        author.setFirstName(firstName.getText().toString());
        author.setLastName(lastName.getText().toString());
        Rate rate = new Rate();
        rate.setNote(note.getText().toString());
        rate.setRatePoints(ratingBar.getRating());
        Opinion opinion = new Opinion();
        opinion.setAuthor(author);
        opinion.setRate(rate);
        return opinion;
    }

    public void sendData(View view){
        final String url = "http://127.0.0.1:8081/opinion";
        Opinion opinion = getData();
        String opionionJson = Utils.parseToJson(opinion);
        Service service = new Service(getApplicationContext());
        service.postOpinion(opionionJson);
    }
}
