package lab4.android.wsb.laboratorium4.model;

/**
 * Created by Bartek on 20.06.2018.
 */

public class Author {

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "author:{" +
                "firstName:" + firstName + ',' +
                "lastName:" + lastName + '}';
    }
}
