package contact;

import com.orm.SugarRecord;

public class Contact extends SugarRecord {

    private String name;
    private String number;

    public void setName(String name){
        this.name = name;
    }

    public void setNumber(String number){
        this.number = number;
    }

    @Override
    public String toString() {
        return "Nazwa: " + name + ", numer: " + number;
    }
}
