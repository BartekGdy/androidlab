package zpum.lab3.laboratorium3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.util.List;

import contact.Contact;

public class MainActivity extends AppCompatActivity {

    EditText name;
    EditText number;
    TextView dbData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.editTextName);
        number = findViewById(R.id.editTextNumber);
        dbData = findViewById(R.id.textViewDBContent);

        SugarContext.init(this);
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());
        showContacts();
    }

    public void addContact(View view) {
        if (name.getText().toString().equals("")||number.getText().toString().equals("")){
            showInvalidDataDialog();
            return;
        }
        Contact contact = new Contact();
        contact.setName(name.getText().toString());
        contact.setNumber(number.getText().toString());
        contact.save();
        clearFields();
        showContacts();
    }

    private void showContacts() {
        List<Contact> contacts = Contact.listAll(Contact.class);
        StringBuilder contactsToShow = new StringBuilder();
        for (Contact contact : contacts) {
            contactsToShow.append(contact).append("\n");
        }
        dbData.setText(contactsToShow.toString());
    }

    public void removeContact(View view) {
        List<Contact> contactByName = Contact.find(Contact.class,"name= ?", name.getText().toString());

        for (Contact contact : contactByName) {
            contact.delete();
        }

        List<Contact> contactByNumber = Contact.find(Contact.class,"number= ?", number.getText().toString());

        for (Contact contact : contactByNumber) {
            contact.delete();
        }

        if (name.getText().toString().equals("") && number.getText().toString().equals("")) {
            Contact.deleteAll(Contact.class);
        }
        showContacts();
        clearFields();
    }

    public void clearFields() {
        number.setText("");
        name.setText("");
    }

    private void showInvalidDataDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Niepełne Dane");
        alertDialog.setMessage("Proszę wprowadzić nazwe i numer");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
